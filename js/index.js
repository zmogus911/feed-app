  google.load("feeds", "1");
  var issaugotiAdresai = [];
  var rusiuojamiPavadinimai = [];
  var rusiuojamaData = [];

  function newTitle(t, d) {
    this.title = t;
    this.date = d;
  }
  //issaugota istorija
  function save() {
    var adresas = document.querySelector('[name="adresas"]').value;
    issaugotiAdresai.push(adresas);
    istorija();
    localStorage.setItem('text', adresas);
  }

  function load() {
    var storedAdress = localStorage.getItem('text');
    if (storedAdress) {
      document.getElementById('kaire').innerHTML += "Last feed you entered -->   " + '<p>' + storedAdress + '</p>';
      initialize3(storedAdress);

    }
  }

  function istorija() {
    var tbody = "";
    for (i = 0; i < issaugotiAdresai.length; i++) {
      tbody += '<p>' + issaugotiAdresai[i] + '</p>';
    }
    document.getElementById('kaire').innerHTML = tbody;
    document.querySelector('[name="adresas"]').value = '';
  }
  //istorijos pabaiga     
  google.setOnLoadCallback(initialize);

  function initialize2() {
    var adresas = document.querySelector('[name="adresas"]').value;
    var feed = new google.feeds.Feed(adresas);
    feed.load(function(result) {
      if (!result.error) {
        for (var i = 0; i < result.feed.entries.length; i++) {
          var entry = result.feed.entries[i];
          //console.log(entry);
          var data = entry.publishedDate;
          var pavadinimas = entry.title;
          var content = entry.content;
          var l = entry.link;
          var vidus = "<h2>" + pavadinimas + "</h2>" + "<br>" + "<p>" + content + "</p>";
          var pavadinimai = '<h4 onclick="modalinis(' + "'" + vidus + "'" + ')' + "," + 'mygtukas(' + "'" + l + "'" + ')' + '" data-toggle="modal" data-target="#m1">' + entry.title + "</h4>" + "<p id='pp'>" + data + "</p>";
          document.getElementById('desine').innerHTML += pavadinimai;
          var a = new newTitle(pavadinimas, data);
          rusiuojamiPavadinimai.push(a);
          console.log(a);
        }
      }
    });
  }

  function modalinis(vidus) {
    document.getElementById('modal-desine').innerHTML = vidus;
  }

  function mygtukas(l) {
    document.getElementById('modal-footer').innerHTML = '<button class="btn go" onclick="pereiti(' + "'" + l + "'" + ')">Go to feed page</button>';
  }

  function pereiti(l) {
    var linkas = l;
    window.open(linkas);
  }

  function initialize3(x) {
    if (x.enght < 1) {
      var adresas = document.querySelector('[name="adresas"]').value;
    } else {
      adresas = x;
    }
    var feed = new google.feeds.Feed(adresas);
    feed.load(function(result) {

      if (!result.error) {

        for (var i = 0; i < result.feed.entries.length; i++) {
          var entry = result.feed.entries[i];
          //console.log(entry);
          var data = entry.publishedDate;
          var pavadinimas = entry.title;
          var content = entry.content;
          var l = entry.link;
          var vidus = "<h2>" + pavadinimas + "</h2>" + "<br>" + "<p>" + content + "</p>";
          //console.log(entry.link);
          var pavadinimai = '<h4 onclick="modalinis(' + "'" + vidus + "'" + ')' + "," + 'mygtukas(' + "'" + l + "'" + ')' + '" data-toggle="modal" data-target="#m1">' + entry.title + "</h4>" + "<p id='pp'>" + data + "</p>";
          document.getElementById('desine').innerHTML += pavadinimai;
          var a = new newTitle(pavadinimas, data);
          rusiuojamiPavadinimai.push(a);
          console.log(a);
        }
      }
    });
  }

  function compareTitle(a, b) {
    if (a.title < b.title) {
      return -1;
    }
    if (a.title > b.title) {
      return 1;
    }
    return 0;
  }

  function compareDate(a, b) {
    if (a.date < b.date) {
      return -1;
    }
    if (a.date > b.date) {
      return 1;
    }
    return 0;
  }

  function sortName() {
    rusiuojamiPavadinimai.sort(compareTitle);

    var rus = '';
    for (var i = 0; i < rusiuojamiPavadinimai.length; i++) {
      rus += '<h4>' + rusiuojamiPavadinimai[i].title + '</h4>' + "<p id='pp'>" + rusiuojamiPavadinimai[i].date + "</p>";

    }
    console.log(rus);
    document.getElementById('desine').innerHTML = rus;

  }

  function sortDate() {
    rusiuojamiPavadinimai.sort(compareDate);
    var rus = '';
    for (var i = 0; i < rusiuojamiPavadinimai.length; i++) {
      rus += '<h4>' + rusiuojamiPavadinimai[i].title + '</h4>' + "<p id='pp'>" + rusiuojamiPavadinimai[i].date + "</p>";

    }
    console.log(rus);
    document.getElementById('desine').innerHTML = rus;
  }